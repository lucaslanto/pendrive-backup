create database autos;

use autos;

create table auto(
	id_Auto int,
	nombre varchar(30),
	marca varchar(20),
	precio int(20),
	primary key(id_Auto)
);

create table venta(
	id_Cant int,
	id_Auto int,
	diasAlquilados int(10),
	pagoEfectuado int(20),
	estado varchar(20),
	primary key(id_Cant),
	foreign key(id_Auto) references auto (id_Auto)
);

create table cliente(
 id_Cliente int,
 nombre varchar(30),
 direccion varchar(30),
 dni int(10),
 primary key(id_Cliente)
);

create table usuario(
  nombreUsuario varchar(30),
  contrasenia varchar(30),
  primary key(nombreUsuario)
);

insert into usuario(nombreUsuario, contrasenia)
values("administrador", "root");

insert into usuario(nombreUsuario, contrasenia)
values("Lucas", "andreslamas2150");


insert into usuario(nombreUsuario, contrasenia)
values("Arispe", "del1al9");

insert into usuario(nombreUsuario, contrasenia)
values("Diego", "hoymasquenunca");