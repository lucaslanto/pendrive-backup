package Paquete;

public class Venta 
{
    private int id_Cant;
    private int id_Auto;
    private int diasAlquilados;
    private int pagoEfectuado;
    private String estado;

    public Venta(int id_Cant, int id_Auto, int diasAlquilados, int pagoEfectuado, String estado) {
        this.id_Cant = id_Cant;
        this.id_Auto = id_Auto;
        this.diasAlquilados = diasAlquilados;
        this.pagoEfectuado = pagoEfectuado;
        this.estado = estado;
    }

    public int getId_Cant() {
        return id_Cant;
    }

    public void setId_Cant(int id_Cant) {
        this.id_Cant = id_Cant;
    }

    public int getId_Auto() {
        return id_Auto;
    }

    public void setId_Auto(int id_Auto) {
        this.id_Auto = id_Auto;
    }

    public int getDiasAlquilados() {
        return diasAlquilados;
    }

    public void setDiasAlquilados(int diasAlquilados) {
        this.diasAlquilados = diasAlquilados;
    }

    public int getPagoEfectuado() {
        return pagoEfectuado;
    }

    public void setPagoEfectuado(int pagoEfectuado) {
        this.pagoEfectuado = pagoEfectuado;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    
}
