package Paquete;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Stack;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;

public class AccesoADatos 
{
    private Connection miConexion;
    private Statement comandos;
    private ResultSet tabla;
    
    public AccesoADatos()
    {
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            miConexion = DriverManager.getConnection("jdbc:mysql://localhost/autos", "root", "");
            comandos = miConexion.createStatement();
        }
        catch (ClassNotFoundException ex) 
        {
            Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (SQLException ex) 
        {
            Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
    
    public void insertarPedido(Venta V)
    {
        String instruccion = "insert into venta values(" + V.getId_Cant()+ ", " + V.getId_Auto() + " , " + V.getDiasAlquilados() + " , " +V.getPagoEfectuado()+ " , '" +V.getEstado() + "')";
        try {
            comandos.executeUpdate(instruccion);
        } catch (SQLException ex) {
            Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, ex);
        }
          
    }
    
    public Vector<String> dameListaAutos()
    {
        Vector<String> aux = new Vector<String>();
        String consulta = "select id_Auto, nombre from auto";
        try {
            tabla = comandos.executeQuery(consulta);
            while(tabla.next())
            {
                String cadena = tabla.getString("id_Auto") + "|" + tabla.getString("nombre");
                aux.add(cadena);
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return aux;
    }
    public void insertarAuto(Auto A)
    {
        String instruccion2 = "insert into auto values(" + A.getid_Auto() + ", '" + A.getNombre() + "', '" +A.getMarca()+"' , '" + A.getPrecio() + "')";
        try 
        {
            comandos.executeUpdate(instruccion2);
        } 
        catch (SQLException ex) 
        {
            Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        public void insertarCliente(Cliente C)
        {
            String instruccion = "insert into cliente values(" + C.getId_Cliente() + " , '" + C.getNombre() + "' , '" + C.getDireccion() + "' , '" + C.getDni()+ "')";
        try {
            comandos.executeUpdate(instruccion);
        } catch (SQLException ex) {
            Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, ex);
        }
            
        }
        public Vector DameListaPedidos()
        {
         
        Vector<Vector<String>> aux = new Vector<Vector<String>>();
        String consulta = "select id_Cant, auto.id_Auto, diasAlquilados, pagoEfectuado, estado from venta inner join auto on venta.id_Auto=auto.id_Auto";
        try {
            tabla = comandos.executeQuery(consulta);
            while(tabla.next())
            {
                Vector<String> Acadena2 = new Vector<String>();
                //String[] Acadena = new String[5];
                //String cadena =tabla.getString("id_Cant")+" "+ tabla.getString("id_Auto")+" "+tabla.getString("diasAlquilados") + " " + tabla.getString("pagoEfectuado")+""+tabla.getString("estado");
                Acadena2.add(tabla.getString("id_Auto"));
                Acadena2.add(tabla.getString("diasAlquilados"));
                Acadena2.add(tabla.getString("pagoEfectuado"));
                Acadena2.add(tabla.getString("estado"));
                aux.add(Acadena2);
            }
        } 
        catch (SQLException ex) 
        {
            Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return aux;
        }
        
        public void IniciarSesion(Usuario U)
        {
            String instruccion = "select nombreUsuario, contrasenia from usuario where nombreUsuario like '" + U.getNombreUsuario() + "' and contrasenia like '" + U.getContrasenia() + "';";
            System.out.println(instruccion);
        try 
        {
            tabla = comandos.executeQuery(instruccion);
            if (tabla.next() == true) 
            {
                Interfaz I = new Interfaz();
                I.setVisible(true);
            }
            else
            {
                JOptionPane.showMessageDialog(null, "Usuario o Contraseña incorrectos");
                Interfaz2 I2 = new Interfaz2();
                I2.setVisible(true);
            }
        } 
        catch (SQLException ex)
        {
            Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
        public void eliminarAuto(Auto A)
        {
            String instrucciones = "delete from auto where id_Auto=" + A.getid_Auto() +";";
            System.out.println(instrucciones);
            try {
                comandos.executeUpdate(instrucciones);
                
            } catch (SQLException e) 
            {
                Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        public void eliminarCliente(Cliente C)
        {
            String instruccions = "delete from cliente where id_Cliente=" + C.getId_Cliente() +";";
            System.out.println(instruccions);
            try {
                comandos.executeUpdate(instruccions);
                
            } catch (SQLException e) 
            {
                Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        public void eliminarVenta(Venta V)
        {
            String instruccions = "delete from venta where id_Auto=" + V.getId_Auto() +";";
            System.out.println(instruccions);
            try {
                comandos.executeUpdate(instruccions);
                
            } catch (SQLException e) 
            {
                Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    public Vector<String> dameListaClientes()
    {
        Vector<String> aux = new Vector<String>();
        String consulta = "select id_Cliente, nombre, direccion, dni from cliente";
        try {
            tabla = comandos.executeQuery(consulta);
            while(tabla.next())
            {
                String cadena = tabla.getString("id_Cliente")+" "+tabla.getString("nombre") + " " + tabla.getString("direccion")+""+tabla.getString("dni");
                aux.add(cadena);
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return aux;
    }
    public Vector<Vector<String>> dameBusqueda()
    {
        Vector<Vector<String>> busq = new Vector<Vector<String>>();
        String consulta = "select id_Auto, nombre from auto";
        try 
        {
         tabla = comandos.executeQuery(consulta);
            while(tabla.next()) 
            {   
             Vector<String> renglon = new Vector<String>();
              renglon.add(tabla.getInt("id_Auto")+" ");
              renglon.add(tabla.getString("nombre"));
            }
        } 
            catch (SQLException ex) 
            {
                Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, ex);
            }
        return busq;
    }
    
    
    public Vector <Vector <String>> dameTabla()      
    {
        Vector<Vector<String>> aux = new Vector<Vector<String>>();
        String consulta = "select * from cliente";
        try {
            tabla = comandos.executeQuery(consulta);
            while(tabla.next())
            {
                Vector<String> renglon = new Vector<String>();
                renglon.add(tabla.getInt("id_Cliente")+" ");
                renglon.add(tabla.getString("nombre"));
                renglon.add(tabla.getString("direccion"));
                renglon.add(tabla.getString("dni"));
                aux.add(renglon);
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return aux;
    }
    public Vector <Vector<String>> dameTablaAutos()
    {
        Vector<Vector<String>> aux2 = new Vector<Vector<String>>();
        String consultar = "select * from auto";
        try 
        {
            tabla = comandos.executeQuery(consultar);
            while(tabla.next()) 
            {
                Vector<String> renglon = new Vector<String>();
                renglon.add(tabla.getInt("id_Auto")+" ");
                renglon.add(tabla.getString("Nombre"));
                renglon.add(tabla.getString("Marca"));
                renglon.add(tabla.getString("Precio"));
                aux2.add(renglon);
            }
        } catch (SQLException ex) 
        {
            Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return aux2;
    }
}


