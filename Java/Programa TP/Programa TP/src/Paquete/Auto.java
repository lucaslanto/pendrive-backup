package Paquete;

public class Auto
{
    private int id_Auto;
    private String nombre;
    private String marca;
    private int precio;

    public Auto(int id_Auto, String nombre, String marca, int precio) {
        this.id_Auto = id_Auto;
        this.nombre = nombre;
        this.marca = marca;
        this.precio = precio;
    }

    public int getid_Auto() {
        return id_Auto;
    }

    public void setid_Auto(int id_Auto) {
        this.id_Auto = id_Auto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }
    
    
}
