package Paquete;

public class Cliente 
{
    private int id_Cliente;
    private String nombre;
    private String direccion;
    private int dni;

    public Cliente(int id_Cliente, String nombre, String direccion, int dni) {
        this.id_Cliente = id_Cliente;
        this.nombre = nombre;
        this.direccion = direccion;
        this.dni = dni;
    }

    public int getId_Cliente() {
        return id_Cliente;
    }

    public void setId_Cliente(int id_Cliente) {
        this.id_Cliente = id_Cliente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }
    
    
}
