package Paquete;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Stack;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;

public class AccesoADatos 
{
    private Connection miConexion;
    private Statement comandos;
    private ResultSet tabla;
    
    public AccesoADatos()
    {
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            miConexion = DriverManager.getConnection("jdbc:mysql://localhost/palabras", "root", "");
            comandos = miConexion.createStatement();
        }
        catch (ClassNotFoundException ex) 
        {
            Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Error de comunicacion con la base de datos. El programa va a apagarse");
            System.exit(0);
        }
        catch (SQLException ex) 
        {
            Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Error de comunicacion con la base de datos. El programa va a apagarse");
            System.exit(0);
        }
   
    }
}