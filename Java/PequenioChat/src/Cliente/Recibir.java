package Cliente;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Recibir extends Thread{
    private Socket socket;
    DataInputStream entrada;
    
    public Recibir(Socket socket){
        this.socket=socket;
        try {
            entrada=new  DataInputStream(socket.getInputStream());
        } catch (IOException ex) {
            Logger.getLogger(Recibir.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }
    public void run(){
        try {
            while(true){
                String renglon=entrada.readUTF();
                System.out.println("Cliente: "+entrada);
            }
            
        } catch (IOException ex) {
            Logger.getLogger(Recibir.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }
    
}
