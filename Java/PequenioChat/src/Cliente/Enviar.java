package Cliente;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Enviar {
    private Socket socket;
    private BufferedReader teclado;
    private DataOutputStream salida;
    
    public Enviar(){
        teclado=new BufferedReader(new InputStreamReader(System.in));
        try {
            socket=new Socket("localhost", 7000);
            salida=new DataOutputStream(socket.getOutputStream());  
           (new Recibir(socket)).start();
            while(true){
                System.out.print("Cliente:");
                String texto=teclado.readLine();
                salida.writeUTF(texto); 
            }
        } catch (IOException ex) {
            Logger.getLogger(Enviar.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    
    }
    public static void main(String[] args) {
        Enviar enviar=new Enviar();
    }
    
    
    
}
