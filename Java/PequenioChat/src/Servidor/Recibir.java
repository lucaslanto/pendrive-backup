/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servidor;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Juanjo
 */
public class Recibir extends Thread{
    private Socket socket;
    private DataInputStream entrada;
    
    public Recibir(Socket socket){
        this.socket=socket;
        try {
            entrada=new  DataInputStream(socket.getInputStream());
        } catch (IOException ex) {
            Logger.getLogger(Recibir.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void run(){
        try {  
            while(true){
                System.out.print("Servidor: ");
                System.out.println(entrada.readUTF());
            }
        } catch (IOException ex) {
            Logger.getLogger(Recibir.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    
}
