package Servidor;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Servidor {
    private ServerSocket servidor;
    private Socket socket;
    private DataOutputStream salida;
    private BufferedReader lector;
    
public Servidor(){
        try {
            servidor=new ServerSocket(7000);
            System.out.println("Esperando cliente");
            socket=servidor.accept();
            salida=new DataOutputStream(socket.getOutputStream());
            (new Recibir(socket)).start();
            lector=new BufferedReader(new InputStreamReader(System.in));            
            while(true){
                System.out.println("Servidor: ");
                String renglon=lector.readLine();
                salida.writeUTF(renglon);       
                           
            }
        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    
}
    public static void main(String[] args) {
        Servidor miServidor=new Servidor();
    }
    
}
