package Paquete;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class Ventana extends JFrame implements ActionListener
{
    public Ventana()
    {
        super("Ventana Deformable");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(1400, 766);
        setLocationRelativeTo(null);
        setVisible(true);
        setResizable(false);
        Container contenedor = this.getContentPane();
        contenedor.setLayout(new GridBagLayout());
        GridBagConstraints GBC = new GridBagConstraints();
        JTextArea Texto = new JTextArea();
        GBC.gridx = 0;
        GBC.gridy = 0;
        GBC.gridwidth = 2;
        GBC.gridheight = 2;
        GBC.weightx = 1.0;
        GBC.weighty = 1.0;
        GBC.fill = GridBagConstraints.BOTH;
        contenedor.add(Texto, GBC);
        /*--------------------------------------------------------*/
        JButton boton1 = new JButton("Boton 1");
        GBC.gridx  = 2;
        GBC.gridy = 1;
        GBC.gridwidth = 1;
        GBC.gridheight = 1;
        GBC.weightx = 0.0;
        GBC.weighty = 0.0;
        GBC.fill = GridBagConstraints.NONE;
        contenedor.add(boton1, GBC);
        
        boton1.addActionListener(this);
        
        /*----------------------------------------------------------*/
        JButton boton2 = new JButton("Boton 2");
        GBC.gridx  = 2;
        GBC.gridy = 1;
        GBC.gridwidth = 1;
        GBC.gridheight = 1;
        GBC.weightx = 0.0;
        GBC.weighty = 0.0;
        GBC.fill = GridBagConstraints.NONE;
        contenedor.add(boton2, GBC);
        /*----------------------------------------------------------*/
        JButton boton3 = new JButton("Boton 3");
        GBC.gridx  = 2;
        GBC.gridy = 1;
        GBC.gridwidth = 1;
        GBC.gridheight = 1;
        GBC.weightx = 0.0;
        GBC.weighty = 0.0;
        GBC.fill = GridBagConstraints.NONE;
        contenedor.add(boton3, GBC);
        /*----------------------------------------------------------*/
        boton3.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Buenas noches", "Ventana tres", JOptionPane.CANCEL_OPTION);
            }
        });
        /*----------------------------------------------------------*/
        
        JTextField texto = new JTextField();
        GBC.gridx = 1;
        GBC.weightx = 1;
        GBC.fill = GridBagConstraints.HORIZONTAL;
        contenedor.add(texto, GBC);
        
        /*----------------------------------------------------------*/
        JButton boton4 = new JButton("Boton 4");
        GBC.gridx  = 2;
        GBC.gridy = 1;
        GBC.gridwidth = 1;
        GBC.gridheight = 1;
        GBC.weightx = 0.0;
        GBC.weighty = 0.0;
        GBC.fill = GridBagConstraints.NONE;
        contenedor.add(boton4, GBC);
        
        setVisible(true);
        /*----------------------------------------------------------*/
        
       
    }
     public void actionPerformed(ActionEvent e)
        {
            JOptionPane.showMessageDialog(null, "Buenos dias", "Ventana", JOptionPane.INFORMATION_MESSAGE);
        }
     
        class Respuesta implements ActionListener
         {
            public void actionPerfmormed(ActionEvent e)
            {
                JOptionPane.showMessageDialog(null, "Buenas tardes", "Segunda Ventana", JOptionPane.ERROR_MESSAGE);
            }


       
         }
    }
        
         

