package paquete;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Servidor {
    private final int PUERTO=2027;
    private final int CANT_CONECTADOS=20;
    private LinkedList<Socket> usuarios=new LinkedList<Socket>();
    
    public void escuchar() {
        try {
            ServerSocket servidor=new ServerSocket(PUERTO,CANT_CONECTADOS);
            while(true){
                System.out.println("Escuchando......");
                Socket cliente=servidor.accept();
                usuarios.add(cliente);
                Runnable run=new HiloServidor(cliente, usuarios);
                Thread hilo=new Thread(run);
                hilo.start();
            
            }
        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static void main(String[] args) {
        Servidor servidor=new Servidor();
        servidor.escuchar();
    }
    
    
    
}
