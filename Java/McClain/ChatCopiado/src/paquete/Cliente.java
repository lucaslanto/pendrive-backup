package paquete;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JEditorPane;


public class Cliente implements Runnable{
    private Socket cliente;
    private DataInputStream in;
    private DataOutputStream out;
    
    private int PUERTO=2027;
    private String host="localhost";
    private String mensaje="";
    JEditorPane panel;
    
    public Cliente(JEditorPane panel) {
        this.panel=panel;
        try {
            cliente=new Socket(host, PUERTO);
            in=new DataInputStream(cliente.getInputStream());
            out=new DataOutputStream(cliente.getOutputStream());
        } catch (IOException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    @Override
    public void run() {
        try {
            while(true){
          
               mensaje += in.readUTF();
               panel.setText(mensaje);
            }
           } catch (IOException ex) {
               Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
           }
       
    }
    
    public void enviarMsg(String msg){
        try {
            out.writeUTF(msg);
        } catch (IOException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
    

