package Paquete;

import javax.swing.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AccesoADatos 
{
   private Connection miConexion;
   private Statement comandos;
   private ResultSet tabla;
   
   public AccesoADatos()
   {
       try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            miConexion = DriverManager.getConnection("jdbc:mysql://localhost/caba", "root", "");
            comandos = miConexion.createStatement();
        }
        catch (ClassNotFoundException ex) 
        {
            Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (SQLException ex) 
        {
            Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, ex);
        }
   }
   public void chat(Chat C)
   {
       ServicioDeChat S = new ServicioDeChat();
       String instruccion = "select respuesta from tabla where nombrePalabra = " + S.TextField.getText();
       try {
           comandos.executeQuery(instruccion);
       } catch (SQLException ex) {
           Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, ex);
       }
   }
   public void eliminarUsuario(Usuario U)
   {
       String instruccion = "delete from usuario where nombreUsuario = '" + U.getNombreUsuario() + "'";
       try {
           comandos.executeUpdate(instruccion);
       } catch (SQLException ex) {
           Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, ex);
       }
   }
    public void insertarUsuario(Usuario U)
        {
            String instruccion = "insert into usuario values('" + U.getNombreUsuario() + "' , '" + U.getContrasenia()+ "')";
        try {
            comandos.executeUpdate(instruccion);
        } catch (SQLException ex) {
            Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, ex);
        }
            
        }
   public void IniciarSesion(Usuario U)
        {
            String instruccion = "select nombreUsuario, contrasenia from usuario where nombreUsuario like '" + U.getNombreUsuario() + "' and contrasenia like '" + U.getContrasenia() + "';";
            System.out.println(instruccion);
        
                try {
                    tabla = comandos.executeQuery(instruccion);
                } catch (SQLException ex) {
                    Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, ex);
                }
                try {
                    if (tabla.next() == true)
                    {
                        Interfaz I = new Interfaz();
                        I.setVisible(true);
                        I.jLabel2.setText("Usuario Conectado: " + U.getNombreUsuario());
                    }
                    else
                    {
                        JOptionPane.showMessageDialog(null, "Usuario o Contraseña incorrectos");
                        Interfaz2 I2 = new Interfaz2();
                        I2.setVisible(true);
                    }   } catch (SQLException ex) {
                    Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, ex);
                }
        } 
  
   
}
