package Paquete;

public class Chat 
{
    private int id_Dato;
    private String nombrePalabra;
    private String respuesta;

    public Chat(int id_Dato, String nombrePalabra, String respuesta) {
        this.id_Dato = id_Dato;
        this.nombrePalabra = nombrePalabra;
        this.respuesta = respuesta;
    }

    public int getId_Dato() {
        return id_Dato;
    }

    public void setId_Dato(int id_Dato) {
        this.id_Dato = id_Dato;
    }

    public String getNombrePalabra() {
        return nombrePalabra;
    }

    public void setNombrePalabra(String nombrePalabra) {
        this.nombrePalabra = nombrePalabra;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }
    
    
}
