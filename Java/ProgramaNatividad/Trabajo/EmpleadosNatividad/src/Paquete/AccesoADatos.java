package Paquete;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Stack;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;

public class AccesoADatos 
{
    private Connection miConexion;
    private Statement comandos;
    private ResultSet tabla;
    
    public AccesoADatos()
    {
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            miConexion = DriverManager.getConnection("jdbc:mysql://localhost/Empleados", "root", "");
            comandos = miConexion.createStatement();
        }
        catch (ClassNotFoundException ex) 
        {
            Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Error de comunicacion. Revise su conexion con la base de datos. El programa va a apagarse");
            System.exit(0);
        }
        catch (SQLException ex) 
        {
            Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Error de comunicacion. Revise su conexion con la base de datos. El programa va a apagarse");
            System.exit(0);
        }
   
        
    }
         public void insertarEmpleado(Empleado E)
    {
        String instruccion = "insert into lista values(" + E.getId_Empleado()+ " , '" + E.getNombreEmp()+ "' , " + E.getEdad()+ " , '" +E.getDireccion()+ "' , '" + E.getRol() + "' , " + E.getDNI()+ " , " + E.getTelefono()+  ")";
        try {
          comandos.executeUpdate(instruccion);
          JOptionPane.showMessageDialog(null, "Empleado/a agregado/a con exito");
        
                     } catch (SQLException ex) 
                     {
                         JOptionPane.showMessageDialog(null, "Error al ingresar un dato, vuelva a revisar");
            Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, ex);
        }
          
    }
             public void insertarUsuario(Usuario U)
    {
        String instruccion = "insert into usuario values('" + U.getNombreUsuario()+ "' , '" + U.getContrasenia() + "')";
        try {
          comandos.executeUpdate(instruccion);
          JOptionPane.showMessageDialog(null, "Usuario Registrado con exito");
          
        
                     } catch (SQLException ex) 
                     {
                         JOptionPane.showMessageDialog(null, "Error al ingresar un dato, vuelva a revisar");
            Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, ex);
        }
          
    }    
             
//     public void dameRespuesta(Ayuda A)
//    {
//        String instruccion = "select palabra, definicion from ayuda where palabra = '" +A.getPalabra() + "' like definicion '" + A.getDefinicion()+"')";
//        try {
//          comandos.executeQuery(instruccion);
//            if (tabla.next()==true) 
//            {
//                AyudaChat AC = new AyudaChat();
//                String pa = AC.AddText.getText();
//                AC.AddText.setText(pa + "\n La palabra buscada es: " + A.getPalabra() + "y la respuesta es: " + A.getDefinicion());
//            }
//          
//        
//                     } 
//        catch (SQLException ex) 
//                     {
//                         JOptionPane.showMessageDialog(null, "La palabra no fue encontrada");
//            Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, ex);
//        }
//          
//    }    
             public void eliminarUsuario(Usuario U)
        {
            String instrucciones = "delete from usuario where nombreUsuario= '" + U.getNombreUsuario() +"';";
           
            try {
                comandos.executeUpdate(instrucciones);
                JOptionPane.showMessageDialog(null, "Usuario: " + U.getNombreUsuario() + " eliminado");
                
            } catch (SQLException e) 
            {
                Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, e);
                JOptionPane.showMessageDialog(null, "No se encontro el usuario especificado, vuelva a intentar");
            }
        }
             
             public Vector dameUsarios()
        {
        Vector<Vector<String>> aux = new Vector<Vector<String>>();
        String consulta = "select nombreUsuario, contrasenia from usuario";
        try {
            tabla = comandos.executeQuery(consulta);
            while(tabla.next())
            {
                Vector<String> Acadena2 = new Vector<String>();
                Acadena2.add(tabla.getString("nombreUsuario"));
                Acadena2.add(tabla.getString("contrasenia"));
                aux.add(Acadena2);
            }
        } 
        
        catch (SQLException ex) 
        {
            Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Error de comunicacion con la base de datos");
        }
        return aux;
        }
             
             public Vector damePalabras()
        {
         Usuario U = new Usuario(null, null);
        Vector<Vector<String>> aux = new Vector<Vector<String>>();
        String consulta = "select palabra from ayuda";
        try {
            tabla = comandos.executeQuery(consulta);
            while(tabla.next())
            {
                Vector<String> Acadena2 = new Vector<String>();
                Acadena2.add(tabla.getString("palabra"));
                aux.add(Acadena2);
            }
        } 
        
        catch (SQLException ex) 
        {
            Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Error de comunicacion con la base de datos");
        }
        return aux;
        } 
             
         public Vector dameEmpleados()
        {
         
        Vector<Vector<String>> aux = new Vector<Vector<String>>();
        String consulta = "select id_Empleado, nombreEmp, edad, direccion, rol, dni, telefono from lista";
        try {
            tabla = comandos.executeQuery(consulta);
            while(tabla.next())
            {
                Vector<String> Acadena2 = new Vector<String>();
                Acadena2.add(tabla.getString("id_Empleado"));
                Acadena2.add(tabla.getString("nombreEmp"));
                Acadena2.add(tabla.getString("edad"));
                Acadena2.add(tabla.getString("direccion"));
                Acadena2.add(tabla.getString("rol"));
                Acadena2.add(tabla.getString("dni"));
                Acadena2.add(tabla.getString("telefono"));
                aux.add(Acadena2);
            }
        } 
        
        catch (SQLException ex) 
        {
            Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Error de comunicacion con la base de datos");
        }
        return aux;
        }
         public Vector dameEmpleadoTipoNombre()
        {
         
        Vector<Vector<String>> aux = new Vector<Vector<String>>();
        String consulta = "select nombreEmp, edad from lista";
        try {
            tabla = comandos.executeQuery(consulta);
            while(tabla.next())
            {
                Vector<String> Acadena2 = new Vector<String>();
                Acadena2.add(tabla.getString("nombreEmp"));
                Acadena2.add(tabla.getString("edad"));
                aux.add(Acadena2);
            }
        } 
        catch (SQLException ex) 
        {
            Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Error de comunicacion con la base de datos");
        }
        return aux;
        }
         public Vector dameEmpleadoTipoDir()
        {
         
        Vector<Vector<String>> aux = new Vector<Vector<String>>();
        String consulta = "select id_Empleado, direccion, edad from lista";
        try {
            tabla = comandos.executeQuery(consulta);
            while(tabla.next())
            {
                Vector<String> Acadena2 = new Vector<String>();
                Acadena2.add(tabla.getString("id_Empleado"));
                Acadena2.add(tabla.getString("direccion"));
                Acadena2.add(tabla.getString("edad"));
                aux.add(Acadena2);
            }
        } 
        catch (SQLException ex) 
        {
            Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Error de comunicacion con la base de datos");
        }
        return aux;
        }
         public Vector dameEmpleadoTipoEmpleo()
        {
         
        Vector<Vector<String>> aux = new Vector<Vector<String>>();
        String consulta = "select id_Empleado, rol, edad from lista";
        try {
            tabla = comandos.executeQuery(consulta);
            while(tabla.next())
            {
                Vector<String> Acadena2 = new Vector<String>();
                Acadena2.add(tabla.getString("id_Empleado"));
                Acadena2.add(tabla.getString("rol"));
                Acadena2.add(tabla.getString("edad"));
                aux.add(Acadena2);
            }
        } 
        catch (SQLException ex) 
        {
            Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Error de comunicacion con la base de datos");
        }
        return aux;
        }
         public Vector dameEmpleadoEdad()
        {
         
        Vector<Vector<String>> aux = new Vector<Vector<String>>();
        String consulta = "select id_Empleado, edad from lista";
        try {
            tabla = comandos.executeQuery(consulta);
            while(tabla.next())
            {
                Vector<String> Acadena2 = new Vector<String>();
                Acadena2.add(tabla.getString("id_Empleado"));
                Acadena2.add(tabla.getString("edad"));
                aux.add(Acadena2);
            }
        } 
        catch (SQLException ex) 
        {
            Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Error de comunicacion con la base de datos");
        }
        return aux;
        }
        
         public void eliminarEmpleado(Empleado E)
        {
            String instrucciones = "delete from lista where id_Empleado=" +E.getId_Empleado()+";";
            System.out.println(instrucciones);
            try {
                comandos.executeUpdate(instrucciones);
                JOptionPane.showMessageDialog(null, "Empleado con el ID: " + E.getId_Empleado() + " eliminado");
                
            } catch (SQLException e) 
            {
                Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, e);
                JOptionPane.showMessageDialog(null, "Error de comunicacion con la base de datos");
            }
        }
         public void IniciarSesion(Usuario U)
        {
            String instruccion = "select nombreUsuario, contrasenia from usuario where nombreUsuario like '" + U.getNombreUsuario() + "' and contrasenia like '" + U.getContrasenia() + "';";
            System.out.println(instruccion);
        try 
        {
            tabla = comandos.executeQuery(instruccion);
            if (tabla.next() == true) 
            {
                InterfazPrincipal I = new InterfazPrincipal();
                I.setVisible(true);
                I.LabelUsuario.setText("Usuario Conectado: " + U.getNombreUsuario());
                }
            else
            {
                JOptionPane.showMessageDialog(null, "Usuario o Contraseña incorrectos");
                InicioSesion I2 = new InicioSesion();
                I2.setVisible(true);
            }
            
            
        } 
        catch (SQLException ex)
        {
            Logger.getLogger(AccesoADatos.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Error de comunicacion con la base de datos");
        }
        }
}