package Paquete;

public class Empleado
{
    private int id_Empleado;
    private String nombreEmp;
    private int edad;
    private String direccion;
    private String rol;
    private int DNI;
    private int telefono;

    public Empleado(int id_Empleado, String nombreEmp, int edad, String direccion, String rol, int DNI, int telefono) {
        this.id_Empleado = id_Empleado;
        this.nombreEmp = nombreEmp;
        this.edad = edad;
        this.direccion = direccion;
        this.rol = rol;
        this.DNI = DNI;
        this.telefono = telefono;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

      public int getDNI() {
        return DNI;
    }

    public void setDNI(int DNI) {
        this.DNI = DNI;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    

    public int getId_Empleado() {
        return id_Empleado;
    }

    public void setId_Empleado(int id_Empleado) {
        this.id_Empleado = id_Empleado;
    }

    public String getNombreEmp() {
        return nombreEmp;
    }

    public void setNombreEmp(String nombreEmp) {
        this.nombreEmp = nombreEmp;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
    
}
