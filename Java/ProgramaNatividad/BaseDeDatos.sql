create database Empleados;

use Empleados;

create table lista
(
	id_empleado int(10),
	nombreEmp varchar(30),
	edad int(30),
	direccion varchar(30),
	rol varchar(30),
	dni int(10),
	telefono int (30),
	primary key(id_empleado)
);

insert into lista(id_Empleado, nombreEmp, edad, direccion, rol, dni, telefono)
values(1, "Lucas", 19, "lamas2150", "Empleado", 40639617, 45812583),
(2, "Arispe", 18, "yatay100", "Empleado", 40687666, 134432843),
(3, "Cristian", 21, "Murature13", "Empleado", 39185836, 15667687),
(4, "Vicky", 18, "HumbertoPrimo", "Gerente", 41997878, 15778779);

create table usuario(

nombreUsuario varchar(30),
contrasenia varchar(30),
primary key(nombreUsuario)
);

insert into usuario (nombreUsuario, contrasenia)
values("administrador", "root");